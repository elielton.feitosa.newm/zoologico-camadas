<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de Usuário</title>
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/cadUsuario.css">
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Cadastro de Usuários</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main>
            <div class="flex box-dados-animal">
                <form id="form-cadastro-cuidador">
                    <fieldset>
                        <legend>Dados do Cuidador</legend>
                        <label for="nome">Nome*</label>
                        <input class="campos-dados" id="campo-nome" type="text" name="nome">
                        <label for="cpf">CPF*</label>
                        <input class="campos-dados" id="campo-usuario" type="text" name="cpf">
                        <label for="responsavel">Responsável por*</label>
                        <select class="campos-dados" id="campo-tipo" name="responsavel">
                        <option>Avestruz</option>
                            <option>Macaco</option>
                            <option>Onça</option>
                            <option>Hipopotamo</option>
                            <option>Urso</option>
                            <option>Girafa</option>
                        </select>
                        <input class="btn-form btn-cadastra" type="button" onClick="enviaCadastroCuidador()" Value="Cadastrar">
                        <a href="?pagina=Home"><input class="btn-form btn-voltar" type="button" value="Voltar"></a>
                        <div class="flex resposta-cadastro"><p id="resposta-conteudo"></p></div>
                    </fieldset>
                </form>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>