<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de Usuário</title>
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/cadUsuario.css">
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Cadastro de Usuários</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main>
            <div class="flex box-dados-animal">
                <form id="form-altera-cuidador">
                    <fieldset>
                        <legend>Dados do Cuidador</legend>
                        <label>ID*</label>
                        <input class="campos-dados" type="number" id="campo-id" name="id" value="<?= strip_tags(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING)) ?>" readonly>
                        <label for="nome">Nome*</label>
                        <input class="campos-dados" id="campo-nome" type="text" name="nome" value="<?= strip_tags(filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING)) ?>" readonly>
                        <label for="cpf">CPF*</label>
                        <input class="campos-dados" id="campo-usuario" type="text" name="cpf" readonly>
                        <label for="responsavel">Responsável por*</label>
                        <select class="campos-dados" id="campo-tipo" name="responsavel">
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Avestruz'){echo 'selected';}else{echo '';} ?>>Avestruz</option>
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Macaco'){echo 'selected';}else{echo '';} ?>>Macaco</option>
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Onça'){echo 'selected';}else{echo '';} ?>>Onça</option>
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Hipopotamo'){echo 'selected';}else{echo '';} ?>>Hipopotamo</option>
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Urso'){echo 'selected';}else{echo '';} ?>>Urso</option>
                        <option <?php if(strip_tags(filter_input(INPUT_GET, 'responsavel', FILTER_SANITIZE_STRING)) == 'Girafa'){echo 'selected';}else{echo '';} ?>>Girafa</option>
                        </select>
                        <input class="btn-form btn-cadastra" type="button" onClick="alteraCuidador()" Value="Cadastrar">
                        <a href="?pagina=Home"><input class="btn-form btn-voltar" type="button" value="Voltar"></a>
                        <div class="flex resposta-cadastro"><p id="resposta-conteudo"></p></div>
                    </fieldset>
                </form>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>