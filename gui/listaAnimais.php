<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/listaAnimais.css">
    <title>Lista de Animais</title>
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Lista de Animais Cadastrados</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main class="flex">
            <table>
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Tamanho(cm)</th>
                        <th>Peso(kg)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(isset($_REQUEST['animais'])){
                            $listaAnimais = $_REQUEST['animais'];
                    
                            foreach($listaAnimais as $animal){
                    ?>
                    <tr>
                        <td><?= $animal['id'] ?></td>
                        <td><?= $animal['nome'] ?></td>
                        <td><?= $animal['tipo'] ?></td>
                        <td><?= $animal['tamanho'] ?></td>
                        <td><?= $animal['peso'] ?></td>
                        <td>
                            <a href="?pagina=AlteraAnimal&id=<?= $animal['id'] ?>&&nome=<?= $animal['nome'] ?>&tipo=<?= $animal['tipo'] ?>&tamanho=<?= $animal['tamanho'] ?>&peso=<?= $animal['peso'] ?>
                            ">
                                <img class="img-lapis" src="public/imagens/lapis.png">
                            </a>
                        </td>
                        <td><img class="img-x" src="public/imagens/x.png" onClick="excluirAnimal(<?= $animal['id'] ?>)"></td>
                        </tr>
                    <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>