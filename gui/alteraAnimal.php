
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/cad-alt-Animal.css">
    <title>Alterar dados do Animal</title>
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Alterar dados do Animal</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main>
            <div class="flex box-dados-animal">
                <form id="form-altera-animal" method="POST">
                    <fieldset>
                        <legend>Dados do Animal</legend>
                        <label for=nome>Id*</label>
                        <input class="campos-dados" type="number" id="id" name="id" value="<?= filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING) ?>" readonly>
                        <label for=nome>Nome*</label>
                        <input class="campos-dados" type="text" id="nome" name="nome" value="<?= filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING) ?>" readonly>
                        <label for="especie">Tipo*</label>
                        <input class="campos-dados" id="tipo" name="tipo"  value="<?= filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING) ?>" readonly>
                        <label for="tamanho">Comprimento(cm)*</label>
                        <input class="campos-dados" id="comprimento" type="text" name="comprimento" value="<?= filter_input(INPUT_GET, 'tamanho', FILTER_SANITIZE_STRING) ?>">
                        <lable for="peso">Peso(kg)*</lable>
                        <input class="campos-dados" type="peso" id="peso" name="peso" value="<?= filter_input(INPUT_GET, 'peso', FILTER_SANITIZE_STRING) ?>">
                        <input type="button" onClick="alteraAnimal()" class="btn-form btn-cadastra" name="btn-cadastra" value="Cadastrar">
                        <a href="?pagina=Home"><input class="btn-form btn-voltar" type="button" value="Voltar"></a>
                        <div class="flex resposta-cadastro"><p id="resposta-conteudo"></p></div>
                    </fieldset>
                </form>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>