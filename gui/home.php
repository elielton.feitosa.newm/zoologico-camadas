<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/home.css">
    <title>Home</title>
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png" alt="Home"></a>
            <h2>Zoológico</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main class="flex">
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/dog.png">
                <div class="flex row">
                    <p>Cadastre um novo animal</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=CadAnimal"><button>Clique aqui</button></a>
                </div>
            </div>
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/pandas.png">
                <div class="flex row">
                    <p>Veja a lista de animais</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=ListaAnimais"><button>Clique aqui</button></a>
                </div>
            </div>
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/protetores.jpg">
                <div class="flex row">
                    <p>Cadastre um novo Cuidador</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=CadCuidador"><button>Clique Aqui</button></a>
                </div>
            </div>
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/cuidador.jpg">
                <div class="flex row">
                    <p>Veja a lista de cuidadores</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=ListaCuidadores"><button>Clique Aqui</button></a>
                </div>
            </div>
            <?php
            
                if($_SESSION['tipo'] == 2){

            ?>
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/user.png">
                <div class="flex row">
                    <p>Cadastrar novo usuário</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=CadUsuario"><button>Clique aqui</button></a>
                </div>
            </div>
            <div class="flex box-acao">
                <img class="img-animais" src="public/imagens/lista-user.jpg">
                <div class="flex row">
                    <p>Veja a lista de usuários</p>
                </div>
                <div class="flex row">
                    <a class="link-btn" href="?pagina=ListaUsuarios"><button>Clique aqui</button></a>
                </div>
            </div>
            <?php

                }

            ?>
        </main>
    </div>
</body>
</html>
