<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/style/geral.css">
    <link rel="stylesheet" href="public/style/listaAnimais.css">
    <title>Lista de Usuários</title>
</head>
<body>
    <div class="flex container">
        <header class="flex">
            <a href="?pagina=Home"><img class="logo-zoo" src="public/imagens/logo.png"></a>
            <h2>Lista de Usuários Cadastrados</h2>
            <a href="?pagina=Sair"><img class="img-logout" src="public/imagens/logout.png" alt="Sair"></a>
        </header>
        <main class="flex">
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Responsaver Por</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(isset($_REQUEST['cuidadores'])){
                            $listaCuidadores = $_REQUEST['cuidadores'];
                    
                            foreach($listaCuidadores as $cuidador){
                    ?>
                    <tr>
                        <td><?= $cuidador['id'] ?></td>
                        <td><?= $cuidador['nome'] ?></td>
                        <td><?= $cuidador['cpf'] ?></td>
                        <td><?= $cuidador['responsavel'] ?></td>
                        <td>
                            <a href="?pagina=AlteraCuidador&id=<?= $cuidador['id'] ?>&nome=<?= $cuidador['nome'] ?>&responsavel=<?= $cuidador['responsavel'] ?>">
                                <img class="img-lapis" src="public/imagens/lapis.png">
                            </a>
                        </td>
                        <td><img class="img-x" src="public/imagens/x.png" onClick="excluirCuidador('<?= $cuidador['id'] ?>')"></td>
                    </tr>
                    <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </main>
    </div>
    <script type="text/javascript" src="public/js/ajax.js"></script>
</body>
</html>