<?php
    session_start();
    if(!isset($_SESSION['tipo'])){
        header('Location: index.php');
    }
    
    define('CWD', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasNoSolid/');

    require_once(CWD . 'business/animalCrud.php');
    require_once(CWD . 'business/usuarioCrud.php');
    require_once(CWD . 'business/cuidadorCrud.php');


    // direciona a página
    if(!empty($_GET['pagina'])){
        $pagina = strip_tags(filter_input(INPUT_GET, 'pagina', FILTER_SANITIZE_STRING));

        switch($pagina){
            case 'Login':
                require_once(CWD . 'gui/login.php');
            break;
            case 'Home':
                require_once(CWD . 'gui/home.php');
            break;
            case 'CadAnimal':
                require_once(CWD . 'gui/cadAnimal.php');
            break;
            case 'AlteraAnimal':
                require_once(CWD . 'gui/alteraAnimal.php');
            break;
            case 'ListaAnimais':
                chamaListaAnimais();
                require_once(CWD . 'gui/listaAnimais.php');
            break;
            case 'CadCuidador':
                require_once(CWD . 'gui/cadCuidador.php');
            break;
            case 'ListaCuidadores':
                chamaListaCuidadores();
                require_once(CWD . 'gui/listaCuidadores.php');
            break;
            case 'AlteraCuidador':
                require_once(CWD . 'gui/alteraCuidador.php');
            break;
            case 'CadUsuario':
                require_once(CWD . 'gui/cadUser.php');
            break;
            case 'AlteraUsuario':
                require_once(CWD . 'gui/alteraUsuario.php');
            break;
            case 'ListaUsuarios':
                chamaListaUsuarios();
                require_once(CWD . 'gui/listaUsuarios.php');
            break;
            case 'Sair':
                require_once('logout.php');
            break;
        }

    }else{
        require_once(CWD . 'gui/home.php');
    }
?>