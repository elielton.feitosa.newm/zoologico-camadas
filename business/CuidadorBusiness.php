<?php
    require_once('IValidar.php');

    class CuidadorBusiness implements IValidar{
        
        private $cuidadorDal;

        public function __construct(){
            $this->cuidadorDal = new CuidadorDal();
        }

        public function cadastro($cuidador){
            if(strlen($cuidador->getNome()) == 0 || strlen($cuidador->getResponsavel()) == 0 || strlen($cuidador->getCpf()) == 0){
                throw new Exception('Preencha todos os Campos');
            }if(strlen($cuidador->getNome()) < 4 || strlen($cuidador->getNome()) > 50){
                throw new Exception('Nome deve ter entre 4 e 50 caracteres');
            }if(strlen($cuidador->getCpf()) != 11){
                throw new Exception('Cpf inválido');
            }

            $resultado = $this->cuidadorDal->cadastrar($cuidador);

            switch($resultado){
                case 1:
                    echo 'Cadastrado com sucesso';
                break;
                case -1:
                    echo 'Erro ao cadastrar';
                break;
            }
        }

        public function alteracao($cuidador){
            if($cuidador->getId() < 1){
                throw new Exception('Id inválido');
            }

            $resultado = $this->cuidadorDal->alterar($cuidador);

            switch($resultado){
                case 1:
                    echo 'Alterado com sucesso';
                break;
                case -1:
                    echo 'Erro ao alterar';
                break;
            }
        }

        public function login($cuidador){
            
        }

    }

?>