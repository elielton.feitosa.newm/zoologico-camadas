<?php
    define('CWN', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasNoSolid/');
    require_once(CWN . 'dal/animalDal.php');
    require_once('AnimalBusiness.php');
    require_once(CWN . 'entity/Animal.php');
    
    $animalDal = new AnimalDal();
    $animalBusiness = new AnimalBusiness();

    $msg = "";
    
    //cadastro de animais
    if(isset($_POST['cadastra'])){
        $animal = new Animal();
        $animal->setNome(strip_tags($_POST['nome']));
        $animal->setTipo(strip_tags($_POST['tipo']));
        $animal->setTamanho(strip_tags($_POST['tamanho']));
        $animal->setPeso(strip_tags($_POST['peso']));

        try{
            $resultado = $animalBusiness->cadastro($animal);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }


    if(isset($_POST['altera'])){
        $animal = new Animal();
        $animal->setId(strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING)));
        $animal->setTamanho(strip_tags(filter_input(INPUT_POST, 'tamanho', FILTER_SANITIZE_STRING)));
        $animal->setPeso(strip_tags(filter_input(INPUT_POST, 'peso', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $animalBusiness->alteracao($animal);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }



    if(isset($_POST['excluir'])){
        $id = strip_tags(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING));

        $resultado = $animalDal->excluirAnimal($id);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir';
            break;
        }

    }

    function chamaListaAnimais(){
        $animalDal = new AnimalDal();
        $_REQUEST['animais'] = $animalDal->listaTodos();
    }
    
?>