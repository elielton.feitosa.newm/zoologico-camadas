<?php
    
    require_once(CWN . 'dal/ConexaoDal.php');
    require_once(CWN . 'business/IValidar.php');

    class AnimalBusiness implements IValidar{
        private $animalDal;
        private $result;
        private $conexaoDal;

        public function __construct(){
            $this->animalDal = new AnimalDal();
            $this->conexaoDal = new ConexaoDal();
        }
        
        function cadastraAnimal(Animal $animal){
            $nome = $animal->getNome();
            $tipo = $animal->getTipo();
            $tamanho = $animal->getTamanho();
            $peso = $animal->getPeso();
            
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = "INSERT INTO animal(nome, tipo, tamanho, peso) VALUES (:nome, :tipo, :tamanho, :peso)";
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(":nome", $nome);
                $stmt->bindParam(":tipo", $tipo);
                $stmt->bindParam(":tamanho", $tamanho);
                $stmt->bindParam(":peso", $peso);
                $result = $stmt->execute();

                if($result){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function cadastro($animal){
        
                if($animal->getNome() == '' || $animal->getTipo() == '' || $animal->getTamanho() == '' || $animal->getPeso() == ''){
                    throw new Exception('Preencha todos os campos');
                }if(strlen($animal->getNome()) > 10 || strlen($animal->getNome()) < 4){
                    throw new Exception('Nome deve ter entre 4 e 10 caracteres');
                }
                
                $resultado = $this->cadastraAnimal($animal);

                switch($resultado){
                    case 1:
                        throw new Exception('Cadastrado com sucesso');
                    break;
                    case -1:
                        throw new Exception('Erro ao cadastrar');
                    break;
                }

                return $resultado;
            
        }

        public function alteracao($animal){
            if($animal->getId() == '' || $animal->getTamanho() == '' || $animal->getPeso() == ''){
                throw new Exception('Preencha todos os campos');
            }
            $resultado = $this->animalDal->alteraAnimal($animal);

            switch($resultado){
                case 1:
                    throw new Exception('Alterado com sucesso');
                break;
                case -1:
                    throw new Exception('Erro ao alterar');
                break;
            }

            return $resultado;
        }

        public function login($obj){
            
        }
    }
?>