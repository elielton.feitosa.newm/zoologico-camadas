<?php
    require_once(CWU . 'dal/UsuarioDal.php');
    require_once(CWU . 'business/IValidar.php');
    class UsuarioBusiness implements IValidar{
        private $usuarioDal;

        public function __construct(){
            $this->usuarioDal = new UsuarioDal();
        }

        public function login($usuario){
            if(strlen($usuario->getUsuario()) == '' || strlen($usuario->getSenha()) == ''){
                throw new Exception('Preencha todos os campos');
            }else{
                $usuario->setSenha(md5($usuario->getSenha()));

                $result = $this->usuarioDal->verificaUsuario($usuario);
                
                switch($result){
                    case -1:
                        throw new Exception('Erro ao verificar login');
                    break;
                    case -2:
                        throw new Exception('Usuário ou senha incorretos');
                    break;
                }
            }
            return $result;
        }

        public function cadastro($usuario){
            if(strlen($usuario->getUsuario()) > 15 || strlen($usuario->getUsuario()) < 4){
                throw new Exception('Usuário deve ter entre 4 e 15 caracteres');
            }if($usuario->getNivel() != 1 && $usuario->getNivel() != 2){
                throw new Exception('Selecione um tipo de usuário válido');
            }if(strlen($usuario->getNome()) > 50 || strlen($usuario->getNome()) < 4){
                throw new Exception('Nome deve ter entre 4 e 50 caracteres');
            }if(strlen($usuario->getUsuario()) == '' || strlen($usuario->getSenha()) == '' || strlen($usuario->getNivel()) == ''){
                throw new Exception('Preencha todos os campos');
            }if(strlen($usuario->getSenha()) > 20 || strlen($usuario->getSenha()) < 8){
                throw new Exception('Senha deve ter entre 8 e 20 caracteres');
            }

            $resultado = $this->usuarioDal->verificaUsuarioExiste($usuario);

            switch($resultado){
                case 1:
                    $usuario->setSenha(md5($usuario->getSenha()));
                    $resultado = $this->usuarioDal->cadastraUsuario($usuario);
                    
                    if($resultado == 1){
                        throw new Exception('Cadastrado com sucesso');
                    }if($resultado == -1){
                        throw new Exception('Erro ao cadastrar usuário');
                    }

                break;
                case -2:
                    throw new Exception('Este usuário já existe, por favor cadastre outro');
                break;
                case -1:
                    throw new Exception('Erro ao verificar se o usuário existe');
                break;
            }
        
        }

        public function alteracao($usuario){
            if($usuario->getUsuario() == '' || $usuario->getNivel() == ''){
                throw new Exception('Os campos(Usuário e tipo devem ser preenchidos)');
            }if($usuario->getSenha() != ''){
                if(strlen($usuario->getSenha()) > 7 && strlen($usuario->getSenha()) < 21){
                    $usuario->setSenha(md5($usuario->getSenha()));
                }else{
                    throw new Exception('Senha deve ter entre 8 e 20 caracteres');
                }
            }

            $resultado = $this->usuarioDal->alteraUsuario($usuario);

            switch($resultado){
                case 1:
                    throw new Exception('Alterado com sucesso');
                break;
                case -1:
                    throw new Exception('Erro ao alterar');
                break;
            }
        }

    }
?>