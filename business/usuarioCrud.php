<?php
    define('CWU', 'C:/xampp/htdocs/TreinamentoNewM/zoologicoCamadasNoSolid/');
    require_once(CWU . 'dal/UsuarioDal.php');
    require_once('UsuarioBusiness.php');
    require_once(CWU . 'entity/Usuario.php');
    $usuarioDal = new UsuarioDal();
    $usuarioBusiness = new UsuarioBusiness();  

    if(isset($_POST['cadastro'])){
        $usuario = new Usuario();
        $usuario->setNome(strip_tags(filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)));
        $usuario->setUsuario(strip_tags(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING)));
        $usuario->setSenha(strip_tags(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)));
        $usuario->setNivel(strip_tags(filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING)));
        
        try{
            $resultado = $usuarioBusiness->cadastro($usuario);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    
    }

    if(isset($_POST['excluir'])){
        $user = strip_tags(filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING));
        
        $resultado = $usuarioDal->excluirUsuario($user);

        switch($resultado){
            case 1:
                echo 'Excluido com sucesso';
            break;
            case -1:
                echo 'Erro ao excluir usuário';
            break;
        }
    }

    if(isset($_POST['altera'])){
        $usuario = new Usuario();
        $usuario->setUsuario(strip_tags(filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING)));
        $usuario->setSenha(strip_tags(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)));
        $usuario->setNivel(strip_tags(filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING)));

        try{
            $resultado = $usuarioBusiness->alteracao($usuario);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }

    }

    function chamaValidacao($user, $pass){
        $usuarioBusiness = new UsuarioBusiness();  
        $usuario = new Usuario();
        $usuario->setUsuario($user);
        $usuario->setSenha($pass);

        $resultado = $usuarioBusiness->login($usuario);

        return $resultado;
    }

    function chamaListaUsuarios(){
        $usuarioDal = new UsuarioDal();
        $_REQUEST['usuarios'] = $usuarioDal->listaTodos();
    }
?>