<?php
    class ConexaoDal{
        private $server = 'localhost';
        private $user = 'root';
        private $password = '';
        private $db = 'zoologico';

        public function conecta(){
            $conexao = new PDO("mysql:server={$this->server}; dbname={$this->db}; charset=utf8", $this->user, $this->password);
            return $conexao;
        }
    }
?>