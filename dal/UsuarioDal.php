<?php
    require_once('ConexaoDal.php');

    class UsuarioDal{
        private $conexaoDal;

        public function __construct(){
            $this->conexaoDal = new ConexaoDal();
        }

        public function verificaUsuario(Usuario $usuario){
            $user = $usuario->getUsuario();
            $pass = $usuario->getSenha();

            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'SELECT usuario, senha, nivel FROM usuario WHERE usuario = :usuario AND senha = :senha';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':usuario', $user);
                $stmt->bindParam(':senha', $pass);
                $result = $stmt->execute();
                $row = $stmt->fetch();

                if($result){
                    if(!empty($row)){
                        return $row;
                    }else{
                        return -2;//usuario nao existe ou senha incorreta
                    }
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo "Erro: " . $ex->getMessage();
            }
        }

        public function verificaUsuarioExiste(Usuario $usuario){
            $user = $usuario->getUsuario();

            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'SELECT usuario FROM usuario WHERE usuario = :usuario';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':usuario', $user);
                $result = $stmt->execute();
                $row = $stmt->fetch();

                if($result){
                    if(!empty($row)){
                        return -2;//Usuario já existe
                    }else{
                        return 1;//usuario nao existe
                    }
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo "Erro: " . $ex->getMessage();
            }
        }

        public function cadastraUsuario(Usuario $usuario){
            $nome = $usuario->getNome();
            $user = $usuario->getUsuario();
            $senha = $usuario->getSenha();
            $nivel = $usuario->getNivel();
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'INSERT INTO usuario(nome, usuario, senha, nivel) VALUES (:nome, :usuario, :senha, :nivel)';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':nome', $nome);
                $stmt->bindParam(':usuario', $user);
                $stmt->bindParam(':senha', $senha);
                $stmt->bindParam(':nivel', $nivel);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOExpception $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function listaTodos(){
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'SELECT nome, usuario, nivel FROM usuario';
                $stmt = $conexao->prepare($sql);
                $resultado = $stmt->execute();

                if($resultado){
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex.getMessage();
            }
        }

        public function excluirUsuario($user){
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'DELETE FROM usuario WHERE usuario = :usuario';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':usuario', $user);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex.getMessage();
            }
        }

        public function alteraUsuario(Usuario $usuario){
            $user = $usuario->getUsuario();
            $senha = $usuario->getSenha();
            $tipo = $usuario->getNivel();

            try{
                $conexao = $this->conexaoDal->conecta();

                if($senha == ''){                    
                    $sql = 'UPDATE usuario SET nivel = :nivel WHERE usuario = :usuario';
                    $stmt = $conexao->prepare($sql);         
                    $stmt->bindParam(':nivel', $tipo);
                    $stmt->bindParam(':usuario', $user);
                }if($senha != ''){
                    $sql = 'UPDATE usuario SET senha = :senha, nivel = :nivel WHERE usuario = :usuario';

                    $stmt = $conexao->prepare($sql);           
                    $stmt->bindParam(':senha', $senha);
                    $stmt->bindParam(':nivel', $tipo);
                    $stmt->bindParam(':usuario', $user);
                }
                
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }
    }
?>