<?php
    
    require_once('ConexaoDal.php');

    class CuidadorDal{
        private $conexaoDal;

        public function __construct(){
            $this->conexaoDal = new ConexaoDal();
        }

        public function cadastrar($cuidador){
            $nome = $cuidador->getNome();
            $cpf = $cuidador->getCPF();
            $responsavel = $cuidador->getResponsavel();

            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'INSERT INTO cuidador(nome, cpf, responsavel) VALUES (:nome, :cpf, :responsavel)';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':nome', $nome);
                $stmt->bindParam(':cpf', $cpf);
                $stmt->bindParam(':responsavel', $responsavel);
                
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function listaTodos(){
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'SELECT * FROM cuidador';
                $stmt = $conexao->prepare($sql);
                $resultado = $stmt->execute();
                $cuidadores = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if($resultado){
                    return $cuidadores;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function alterar($cuidador){
            $id = $cuidador->getId();
            $responsavel = $cuidador->getResponsavel();
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'UPDATE cuidador SET responsavel = :responsavel WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':responsavel', $responsavel);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }

        public function excluir($cuidador){
            $id = $cuidador->getId();
            try{
                $conexao = $this->conexaoDal->conecta();
                $sql = 'DELETE FROM cuidador WHERE id = :id';
                $stmt = $conexao->prepare($sql);
                $stmt->bindParam(':id', $id);
                $resultado = $stmt->execute();

                if($resultado){
                    return 1;
                }else{
                    return -1;
                }

            }catch(PDOException $ex){
                echo 'Erro: ' . $ex->getMessage();
            }
        }
    }

?>